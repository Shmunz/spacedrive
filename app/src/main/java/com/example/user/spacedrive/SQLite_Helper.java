package com.example.user.spacedrive;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by User on 17/03/2017.
 */

public class SQLite_Helper extends SQLiteOpenHelper {
    public static final String DATABASENAME="Players.db";
    public static final String TABLE_Players="tblPlayer";
    public static final int DATABASEVERSION=1;
    private SQLiteDatabase data;
    public final String[] ALL_COLUMBS=new String[]{COLUMN_ID,COLUMN_NAME,COLUMN_SCORE};

    public static final String COLUMN_ID="playerId";
    public static final String COLUMN_NAME="playerName";
    public static final String COLUMN_SCORE="Score";

    private static final String  CREATE_TABLE_Players="CREATE TABLE IF NOT EXISTS " +
            TABLE_Players + "(" + COLUMN_ID +  " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_NAME + " VARCHAR," + COLUMN_SCORE +" INTEGER);";

    public SQLite_Helper(Context context) {
        super(context,DATABASENAME,null,DATABASEVERSION);
    }
    public void reset(){
        data.execSQL("DROP TABLE IF EXISTS "+TABLE_Players);
        onCreate(data);
    }
    public void open(){
        data=this.getWritableDatabase();
    }
    public void addPlayer(player player){
        ContentValues cv=new ContentValues();
        cv.put(COLUMN_NAME,player.getName());
        cv.put(COLUMN_SCORE,player.getScore());
        data.insert(TABLE_Players,null,cv);
    }
    public ArrayList<player> getFromData(){
        Cursor cursor=data.query(TABLE_Players,ALL_COLUMBS, null, null, null, null, null);
        ArrayList<player> info=new ArrayList<player>();
        for(cursor.moveToFirst();!cursor.isAfterLast();cursor.moveToNext()){
            String name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME));
            int rank = cursor.getInt(cursor.getColumnIndex(COLUMN_SCORE));
            info.add(new player(name,rank));
        }
        cursor.close();
        return info;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_Players);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
