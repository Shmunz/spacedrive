package com.example.user.spacedrive;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.Log;

/**
 * Created by User on 28/12/2016.
 */

public class Ship {
    float x,y;
    float []speed;
    Bitmap image;

    public Ship(int x, int y, float[]speed, Resources res){
        image= BitmapFactory.decodeResource(res,R.drawable.space_ship);
        this.x=x;
        this.y=y;
        this.speed=speed;
        //Log.i("backdrawing","ship");
    }
    public void update(int Pwidth,int Pheight){
        x=BackView.cap(0,Pwidth-image.getWidth(),x+speed[0]*5);
        //Log.i("backdrawing",""+x);
    }
    public boolean isCollision(Point p){
        if(p.x>=this.x && p.x<=this.x+this.image.getWidth()){
            if(p.y>=this.y && p.y<=this.y+this.image.getHeight())
                return true;
        }
        return false;
    }
}
