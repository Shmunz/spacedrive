package com.example.user.spacedrive;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by User on 28/12/2016.
 */

public class Star extends GameItem {
    Paint paint;
    float rad;

    public Star(int x,int y,int[]speed){
        this.x=x;
        this.y=y;
        this.speed=speed;
        this.paint=new Paint();
        paint.setColor(Color.WHITE);
        rad=speed[1]/3.5F;
    }
    public void update(){
        this.x+=speed[0];
        this.y+=speed[1];
    }
}
