package com.example.user.spacedrive;

import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by User on 08/01/2017.
 */

public class shoot{
    float []speed;
    float x,y;
    Paint paint;

    public shoot(GameItem parent,int dirX,int dirY){
        this.x=parent.x;
        this.y=parent.y;
        this.speed=new float[2];
        this.speed[0]=dirX-this.x;
        this.speed[1]=dirY-this.y;
        speed=setConstVel(speed[0],speed[1]);
        speed[0]*=10;
        speed[1]*=10;
        this.paint=new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.FILL);
    }
    public shoot(Ship parent,int dirX,int dirY){
        this.x=parent.x+(parent.image.getWidth()/2);
        this.y=parent.y;
        this.speed=new float[2];
        this.speed[0]=dirX-this.x;
        this.speed[1]=dirY-this.y;
        speed=setConstVel(speed[0],speed[1]);
        speed[0]*=10;
        speed[1]*=10;
        this.paint=new Paint();
        paint.setColor(Color.YELLOW);
        paint.setStyle(Paint.Style.FILL);
    }

    private float[] setConstVel(float x1,float y1) {
        if(Math.pow(x1,2)+Math.pow(y1,2)<=16)
            return new float[]{x1,y1};
        else return setConstVel(x1/2,y1/2);
    }

    void update() {
        this.x+=speed[0];
        this.y+=speed[1];
    }
}
