package com.example.user.spacedrive;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;

import java.util.Random;

public class Astroid extends GameItem {
    int size;
    Bitmap icon;

    public Astroid(int x,int y,int speed[],int size,Resources res){
        this.x=x;
        this.y=y;
        icon= Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res,R.drawable.astero), size, size, false);
        /*this.paint=new Paint();
        paint.setColor(Color.GRAY);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(8F);*/
        this.speed=speed;
        this.size=size;
    }
    public void update(){
        x+=speed[0];
        y+=speed[1];
    }
}
