package com.example.user.spacedrive;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class about extends AppCompatActivity implements View.OnClickListener {
    TextView tv;
    String text;
    Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        text="Well in every game there is a story, so here is one.\nYou are a red space ship trying to fly in space.\nYou entered an asteroid belt.\nGood for you. Now here is how to play the game.\n" +
                "\nIt's pretty strait forward: \n\n1. You fly in space. \n2. Asteroids fly in space. \n3. Dont hit your ship with the asteroids. \n4. Oh you can shoot by taping the screen, so shoot.\n" +
                "5. If you destroy asteroids, you will get extra points. \n6.Fly as long as you can. \n7. Tilt your phone to move.\n8. Have fun :3\n\nCreated by Shon Feldman\nDesined by Shon Feldman" +
                "\nCoded by Shon Feldman";
        tv=(TextView)findViewById(R.id.info);
        tv.setText(text);
        back=(Button)findViewById(R.id.fin);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(back))
            this.finish();
    }
}
