package com.example.user.spacedrive;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class LeaderBoard extends AppCompatActivity {
    ListView lv;
    SQLite_Helper helper;
    ArrayList<player> info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        lv=(ListView)findViewById(R.id.lv);
        helper=new SQLite_Helper(this);
        helper.open();
        info=helper.getFromData();
        info=sort(info);
        lv.setAdapter(new PlayerAdapter(this,0,info));
        helper.close();
    }
    public ArrayList<player> sort(ArrayList<player> arr) {
        ArrayList<player> temp = new ArrayList<player>();
        while (arr.size() != 0){
            player max = arr.get(0);
            for (player p : arr) {
                if (max.getScore() < p.getScore())
                    max = p;
            }
        temp.add(max);
        arr.remove(max);
        }
    return temp;
    }
}
