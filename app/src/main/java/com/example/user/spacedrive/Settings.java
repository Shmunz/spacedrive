package com.example.user.spacedrive;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Settings extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    Spinner spin;
    TextView tv;
    Button back,save,notify;
    SharedPreferences settings;
    boolean isUp;
    int diff;
    BroadCastBattery broadCastBattery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        broadCastBattery=new BroadCastBattery();
        ArrayAdapter<String> data=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,new String[]{"Baby Mode","Okey","Hellish","OMGWTFRUFOREAL"});
        data.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin=(Spinner)findViewById(R.id.spinner);
        spin.setAdapter(data);
        tv=(TextView)findViewById(R.id.battry);
        spin.setOnItemSelectedListener(this);
        back=(Button)findViewById(R.id.back);
        back.setOnClickListener(this);
        isUp=false;
        notify=(Button) findViewById(R.id.service);
        notify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonPlayClick();
            }
        });
        save=(Button)findViewById(R.id.save);
        save.setOnClickListener(this);
        settings=getSharedPreferences("settings",0);
        if(!settings.contains("speed")){
            SharedPreferences.Editor editor=settings.edit();
            editor.putInt("speed",1000);
            editor.commit();
        }
        setSpinner();

    }

    private void buttonPlayClick() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if(!isUp){
            isUp=true;
            notify.setText("prees to stop");
            int icon = R.drawable.space_ship;
            String ticket = " press to start playing";
            long when = System.currentTimeMillis();
            String title = "Ready?";
            String ticker = "ticker";
            String text="press to start playing!!";
            Intent intent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());
            Notification notification = builder.setContentIntent(pendingIntent)
                    .setSmallIcon(icon).setTicker(ticker).setWhen(when)
                    .setAutoCancel(true).setContentTitle(title).
                            setSmallIcon(R.drawable.space_ship)
                    .setContentText(text).build();
            notificationManager.notify(1, notification);
        }
        else{
            isUp=false;
            notify.setText("press");
            notificationManager.cancel(1);
        }
    }

    private class BroadCastBattery extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {
            int battery = intent.getIntExtra("level",0);
            tv.setText(String.valueOf(battery) + "%");


        }
    }


    private void setSpinner() {
        switch(settings.getInt("speed",1000)){
            case 1000:spin.setSelection(0);return;
            case 500:spin.setSelection(1);return;
            case 250:spin.setSelection(2);return;
            case 150:spin.setSelection(3);return;
            default:return;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(back))
            this.finish();
        else if(v.equals(save)){
            SharedPreferences.Editor editor=settings.edit();
            editor.putInt("speed",diff);
            editor.commit();
            Toast.makeText(this,"The changes were saved",Toast.LENGTH_SHORT).show();
            this.finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadCastBattery,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadCastBattery);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch(position){
            case 0: diff=1000;return;
            case 1: diff=500;return;
            case 2: diff=250;return;
            case 3: diff=150;return;
            default:return;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
