package com.example.user.spacedrive;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Random;



public class BackView extends View{
    private LinearLayout.LayoutParams ly;
    private Context context;
    private boolean TimerOn,FpsTimer=true;//killing timer threads if false
    private Random rand=new Random();
    static final int FPS=60;
    private GameTread FPSthread;
    private GameTimer timer,astroTimer,FireRate;
    private ArrayList<Star> StarArr;
    private ArrayList<shoot> shoots;
    private int sleep;
    private int time=0;
    private boolean isUp=false,Drawn=false,shotIsUp=true;//init game draw
    private Handler handler;
    private boolean astroFlag=true;//timed to spawn asteroids
    private Ship player;
    private float velship;
    private ArrayList<Astroid> asteriods;

    public static float cap(float min, float max, float value){
        if(value<min)
          return min;
        else if(value>max)
            return max;
        else return value;
    }

    public BackView(Context context, int width, int height,int sleep) {
        super(context);
        this.TimerOn=true;
        this.sleep=sleep;
        this.context=context;
        ly=new LinearLayout.LayoutParams(width,height);
        this.setLayoutParams(this.ly);
        init();
    }

    private void init() {
        rand = new Random();
        FPSthread=new GameTread();
        FPSthread.start();
        asteriods=new ArrayList<Astroid>();
        shoots=new ArrayList<shoot>();
        timer=new GameTimer(1000,1);
        timer.start();
        astroTimer=new GameTimer(sleep,2);
        astroTimer.start();
//        FireRate=new GameTimer(1000,3,true);
//        FireRate.start();
        StarArr = new ArrayList<Star>();
        handler=new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                switch (msg.what){
                    case 0:
                        invalidate();return true;
                    case 1:
                        time+=1;return true;
                    case 2:
                        astroFlag=true;return true;
                    case 3:
                        shotIsUp=true;
                        return true;
                    default:return true;
                }
            }
        });
        isUp=true;
    }
    public int getScore(){
        return time;
    }
    public void setValue(float vel){
        velship=vel;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int evn=event.getAction();
        if(evn==MotionEvent.ACTION_DOWN && isUp){
            shoots.add(new shoot(player,(int)event.getX(),(int)event.getY()));
            //Log.i("backdrawing","in,added");
        }
        return super.onTouchEvent(event);
    }
    public boolean isStop(){
        return FpsTimer;
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //new paint
        Paint paint=new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(150F);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        //text draw
        canvas.drawText(""+time,getWidth()/2-70,150,paint);
        paint.reset();

        if(!Drawn){
            firsrDraw();
            Drawn=true;
        }

        player.update(getWidth(),getHeight());
        player.speed[0]=velship;
        canvas.drawBitmap(player.image,player.x,player.y,null);

        for (Star s : StarArr) {//star update system
            if(s.y>getHeight())
                s.y=-10;
            else if(s.x>getWidth())
                s.x=0;
            else if(s.x<0)
                s.x=getWidth();
            canvas.drawCircle(s.x,s.y,s.rad,s.paint);
            s.update();
        }//end star*

        astroMaker();//start of asteroid

        for(Astroid astro:asteriods){
            canvas.drawBitmap(astro.icon,astro.x,astro.y,null);
            //Log.i("backdrawing","in doing "+astro.x+" "+astro.y);
            astro.update();
            if(shipAstroCollis(astro)){
                TimerOn=false;
                FpsTimer=false;
            }
        }
        for(int i=0;i<asteriods.size();i++){
            if(asteriods.get(i).y>getHeight())
                asteriods.remove(i);
            //Log.i("backdrawing","entetis "+asteriods.size());
        }
        paint.reset();

        //start of shot
        for(shoot shot:shoots){
            canvas.drawCircle(shot.x,shot.y,20,shot.paint);
            shot.update();
        }
        for(int i=0;i<shoots.size();i++){
            shoot shot=shoots.get(i);
            if(shot.y<0 || shot.x<0 || shot.x>getWidth() || shot.y>getHeight())
                shoots.remove(i);
            else {
                for (int j=0;j<asteriods.size();j++) {
                    if (inside(shot,asteriods.get(j))) {
                        asteriods.remove(j);
                        shoots.remove(shot);
                        time+=10;
                    }
                }
            }
            //Log.i("backdrawing","entetis "+asteriods.size());
        }
        updateStarWithShip();
    }
    private boolean shipAstroCollis(Astroid astro){//checking for collision
        Point[]sides=new Point[4];//left top,right top,left bottom,right bottom
        sides[0]=new Point(astro.x,astro.y);
        sides[1]=new Point(astro.x+astro.size,astro.y);
        sides[2]=new Point(astro.x,astro.y+astro.size);
        sides[3]=new Point(astro.x+astro.size,astro.y+astro.size);

        return(player.isCollision(sides[0]) || player.isCollision(sides[1]) || player.isCollision(sides[2]) || player.isCollision(sides[3]));
    }
    private boolean inside(shoot shot,Astroid astro){
        return (shot.x>=astro.x && shot.x<=astro.x+astro.size && shot.y>=astro.y && shot.y<=astro.y+astro.size);
    }

    private void updateStarWithShip() {
        for(Star s:StarArr){
            if( player.speed[0]!=0)
                s.speed[0]= ((int) - player.speed[0])*s.speed[1]/5;
            else s.speed[0]=0;
        }
    }

    private void firsrDraw() {
        for(int i=0;i<75;i++)
            StarArr.add(new Star(rand.nextInt(getWidth()), rand.nextInt(getHeight()), new int[]{0,rand.nextInt(23)+11}));
        player=new Ship((getWidth()/2)-200,getHeight()-250,new float[]{0,0},getResources());
       // asteriods.add(new Astroid(getWidth()/2,0,new int[]{0,5},200));
    }
    private void astroMaker(){
        if(astroFlag){
            int place=rand.nextInt(3);//0-left,1-top,2-right
            switch(place){
                case 0:
                    asteriods.add(new Astroid(-200,rand.nextInt(getHeight()/2),new int[]{rand.nextInt(15)+5,rand.nextInt(15)+10},rand.nextInt(200)+50,getResources()));astroFlag=false;return;
                case 1:
                    asteriods.add(new Astroid(rand.nextInt(getWidth()),-200,new int[]{rand.nextInt(4)+5,rand.nextInt(15)+10},rand.nextInt(200)+50,getResources()));astroFlag=false;return;
                case 2:
                    asteriods.add(new Astroid(getWidth(),rand.nextInt(getHeight()/2),new int[]{-rand.nextInt(15)-5,rand.nextInt(15)+10},rand.nextInt(200)+50,getResources()));astroFlag=false;return;
                default:return;
            }
        }
    }


    private class GameTread extends Thread{
        long startime;
        long stepepersec=1000/FPS;
        long sleeptime;

        @Override
        public void run() {
            super.run();
            while (FpsTimer){
                startime=System.currentTimeMillis();
                sleeptime=stepepersec-(System.currentTimeMillis()-startime);
                try {
                    if(sleeptime>0)
                        sleep(sleeptime);
                    else sleep(10);
                }catch (InterruptedException e) {
                    e.printStackTrace();
                }

                handler.sendEmptyMessage(0);
            }//end of while
        }
    }

    public class GameTimer extends Thread{
        int MiliSectoSleep;
        int name;
        boolean oneTimeRun;

        public GameTimer(int MiliSectoSleep,int name){
            this.MiliSectoSleep=MiliSectoSleep;
            this.name=name;
            this.oneTimeRun=false;
        }
        public GameTimer(int MiliSectoSleep,int name,boolean bool){
            this.MiliSectoSleep=MiliSectoSleep;
            this.name=name;
            this.oneTimeRun=bool;
        }

        @Override
        public void run() {
            super.run();
            while(TimerOn && !oneTimeRun){
                try {
                    sleep(MiliSectoSleep);
                    handler.sendEmptyMessage(this.name);
                } catch (InterruptedException e) {
                    e.printStackTrace();}
            }
//            if(oneTimeRun) {
//                try {
//                    sleep(MiliSectoSleep);
//                    handler.sendEmptyMessage(this.name);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();}
//            }
            if(!TimerOn){
                try {
                    this.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
