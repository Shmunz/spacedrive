package com.example.user.spacedrive;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements SensorEventListener, View.OnClickListener {
    private BackView bv;
    private LinearLayout layout;
    private SensorManager senSensorManager;
    Dialog d;
    looping loop;
    Button b1,b2,b3,b4,b5;
    boolean ready=false;
    private Handler handler;
    TextView tv;
    EditText et;
    player p;
    SQLite_Helper helper;
    SharedPreferences settings;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layout=(LinearLayout)findViewById(R.id.activity_main);
        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        handler=new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                if(msg.what==0){
                    Fdial();
                    try {
                        loop.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return false;
            }
        });
        Rdial();
        settings=getSharedPreferences("settings",0);
        if(!settings.contains("speed")){
            SharedPreferences.Editor editor=settings.edit();
            editor.putInt("speed",1000);
            editor.commit();
        }
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu,menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case R.id.on:
//                this.sensorOn=true;getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);;return true;
//        default:return super.onOptionsItemSelected(item);}
//    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY |View.SYSTEM_UI_FLAG_FULLSCREEN |View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        senSensorManager.registerListener(this, senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    void Rdial(){
        d=new Dialog(this);
        d.setCancelable(false);
        d.setContentView(R.layout.ready_ly);
        //d.setContentView(getLayoutInflater().inflate(R.layout.ready_ly,null),new LinearLayout.LayoutParams(5000,1000));
        b1=(Button)d.findViewById(R.id.button);
        b2=(Button)d.findViewById(R.id.button2);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        d.show();
    }
    void Sdial(int score){
        d=new Dialog(this);
        d.setCancelable(false);
        d.setContentView(R.layout.input_name);
        tv=(TextView)d.findViewById(R.id.textView23);
        tv.setText("Your score: "+score);
        et=(EditText)d.findViewById(R.id.editname);
        b3=(Button)d.findViewById(R.id.done);
        b5=(Button)d.findViewById(R.id.sane);
        b3.setOnClickListener(this);
        b5.setOnClickListener(this);
        d.show();
    }
    void Fdial(){
        d=new Dialog(this);
        d.setCancelable(false);
        d.setContentView(R.layout.ready_ly);
        tv=(TextView)d.findViewById(R.id.textView);
        tv.setText("You Died X_X");
        b3=(Button)d.findViewById(R.id.button);
        b3.setText("To menu");
        b4=(Button)d.findViewById(R.id.button2);
        b4.setText("Save score");
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        d.show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor mySensor = event.sensor;
        if (ready && mySensor.getType() == Sensor.TYPE_ACCELEROMETER)
            bv.setValue(-event.values[0]);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onClick(View v) {
        if(v.equals(b1)||v.equals(b2)) {
            d.dismiss();
            int diff=settings.getInt("speed",1000);
            bv=new BackView(this, ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT,diff);
            layout.addView(bv);
            ready = true;
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            senSensorManager.registerListener(this, senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
            loop=new looping();
            loop.start();
        }
        else if (v.equals(b4)){
            d.dismiss();
            Sdial(bv.getScore());
        }
        else if (v.equals(b5)){
            if (!et.getText().toString().equals("")){
                p=new player(et.getText().toString(),bv.getScore());
                helper=new SQLite_Helper(this);
                helper.open();
                helper.addPlayer(p);
                helper.close();
                startActivity(new Intent(this,LeaderBoard.class));
                this.finish();
            }
        }
        else if(v.equals(b3)){
            d.dismiss();
            this.finish();
        }
    }
    private class looping extends Thread{
        @Override
        public void run() {
            super.run();
            while(bv.isStop()){
                //Log.i("backdrawing","running");
            }
            handler.sendEmptyMessage(0);
        }
    }
}
