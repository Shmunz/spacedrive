package com.example.user.spacedrive;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu extends AppCompatActivity implements View.OnClickListener {
    Button play,about,settings,exit,led;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        init();
    }

    private void init() {
        play=(Button)findViewById(R.id.Play);
        play.setOnClickListener(this);
        settings=(Button)findViewById(R.id.Settings);
        settings.setOnClickListener(this);
        about=(Button)findViewById(R.id.about);
        about.setOnClickListener(this);
        led=(Button)findViewById(R.id.score);
        led.setOnClickListener(this);
        exit=(Button)findViewById(R.id.Exit);
        exit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(play))
            startActivity(new Intent(this,MainActivity.class));
        else if(v.equals(settings))
            startActivity(new Intent(this,Settings.class));
        else if(v.equals(about))
            startActivity(new Intent(this,about.class));
        else if(v.equals(led))
            startActivity(new Intent(this,LeaderBoard.class));
        else if(v.equals(exit))
            this.finish();
    }
}
