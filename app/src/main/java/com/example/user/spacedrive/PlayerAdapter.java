package com.example.user.spacedrive;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by User on 01/04/2017.
 */

public class PlayerAdapter extends ArrayAdapter<player> {
    Context context;
    List<player> list;

    public PlayerAdapter(Context context, int resource, List<player> objects) {
        super(context, resource, objects);
        this.context=context;
        this.list=objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v=((Activity)context).getLayoutInflater().inflate(R.layout.tablesql_ly,null);
        TextView num=(TextView)v.findViewById(R.id.num);
        TextView name=(TextView)v.findViewById(R.id.name);
        TextView score=(TextView)v.findViewById(R.id.score23);
        num.setText((position+1)+".");
        player p=list.get(position);
        name.setText(p.getName());
        score.setText(p.getScore()+"");
        return v;
    }
}
