package com.example.user.spacedrive;

/**
 * Created by User on 17/03/2017.
 */

public class player implements info {
    String name;
    int score;

    public player(String name,int score){
        this.name=name;
        this.score=score;
    }
    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public int getScore() {
        return this.score;
    }
}
